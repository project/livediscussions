<?php
/**
  * @file
  * Creates a block of links to the nodes most recently commented on
  * Number of links in the block and block title is configurable
  *
*/
function live_discussions_help($path, $arg) {
  switch($path) {
    case 'admin/block/help':
      return t('Live discussions');
      break;
    case "admin/modules#description":
      $output = "Create a block that lists the nodes most recently commented on";
      break;
    case "admin/help#live%20discussions":
      $output = t('<p>The Live Discussions module creates a block listing recently commented upon threads. The number of nodes listed in the block is configurable (5, 10, 15, 20, 25 or 30 nodes). The link can display the number of comments made on the node and the block title is configurable.</p><p>(This is different than the block provided in the core distribution because it lists nodes with comments rather than comments)</p>');
    default:
      $output = "";
      break;
  }
  return $output;
}


function live_discussions_perm(){
  return array('administer live discussions block', 'access live discussion page');
}

function live_discussions_block($op='list', $delta=0, $edit=array()) {
  $_live_discussion_config = variable_get('live_discussion_config', array());
  switch ($op) {
    case 'list': {
      $block[0]["info"] = t("Live discussions");
      return $block;
      break;
    }
    case 'configure': {
      $output['live_discussion_title'] = array(
      	'#type' => 'textfield',
      	'#title' => t('Title '),
      	'#default_value' => $_live_discussion_config['live_discussion_title'] ? $_live_discussion_config['live_discussion_title'] : 'Recent comments',
      	'#size' => 64,
      	'#maxlength' => 64
    	);
      $output['live_discussion_link_count'] = array(
        '#type' => 'select',
        '#title' => t('Number of recently commented posts to display'),
        '#default_value' => $_live_discussion_config['live_discussion_link_count'] ? $_live_discussion_config['live_discussion_link_count'] : 3,
        '#options' => drupal_map_assoc(array(5,10,15,20,25,30)),
        '#attributes' => 'size="4"'
      );

      $output['live_discussion_show_count'] = array(
      	'#type' => 'radios',
      	'#title' => t('Show comment count per post'),
      	'#default_value' => $_live_discussion_config['live_discussion_show_count'] ? $_live_discussion_config['live_discussion_show_count'] : 'Yes',
        '#options' => array('Yes'=>'Yes', 'No' => 'No'),
    	);
      return $output;
      break;
    }
    case 'save': {
      $_live_discussion_config['live_discussion_title'] = $edit['live_discussion_title'];
      $_live_discussion_config['live_discussion_link_count'] = $edit['live_discussion_link_count'];
      $_live_discussion_config['live_discussion_show_count'] = $edit['live_discussion_show_count'];
      variable_set('live_discussion_config', $_live_discussion_config);
      break;
    }
    case 'view': {
      // our block content
      $block_content = '';
      // create block contents
      $show_comment_count = $_live_discussion_config['live_discussion_show_count'] ? $_live_discussion_config['live_discussion_show_count'] :  'Yes';
      $commented_limit = $_live_discussion_config['live_discussion_link_count'] ? $_live_discussion_config['live_discussion_link_count'] : 15;
      if (arg(0) =='blog'){
        $select_blog = "AND n.type = 'blog'";
        if (arg(1)) $select_blog .= " AND n.uid = '".arg(1)."' ";
      }

      // create the SQL
      $query = "SELECT cs.nid, n.title, cs.comment_count, cs.last_comment_timestamp
               FROM {node_comment_statistics} cs
               LEFT JOIN {node} n on n.nid = cs.nid
               WHERE n.status = 1 AND cs.comment_count > 0
               $select_blog
      				 ORDER BY cs.last_comment_timestamp DESC";

      $query_result = db_query_range(db_rewrite_sql($query), 0, $commented_limit);

//      if (db_num_rows($query_result) > 0) {
        $comments_per_page = $user->comments_per_page ? $user->comments_per_page : ($_SESSION['comment_comments_per_page'] ? $_SESSION['comment_comments_per_page'] : variable_get('comment_default_per_page', '50'));
        // for each record create a link
        while ($outnode = db_fetch_object($query_result)){
          $start_comment = $outnode->comment_count - ($outnode->comment_count % $comments_per_page);
          $link_q = ($comments_per_page < $outnode->comment_count) ? "from=$start_comment&comments_per_page=$comments_per_page" : NULL;
          $linktext[] = l(
            $outnode->title.($show_comment_count == 'Yes' ? ' ('.$outnode->comment_count.')':''),
            "node/$outnode->nid", array(),
            $link_q,
            "comment");
//        }
				if (count($linktext)) {
	        $block['content'] = theme_item_list($linktext);
	        $block['subject'] = $_live_discussion_config['live_discussion_title'] ? $_live_discussion_config['live_discussion_title'] : 'Live Discussions';
				}
      }
      return $block;
    }
  }
}

function live_discussions_menu() {
  $items['live_discussions'] = array(
  	'title' => 'Live discussions',
    'page callback' => 'live_discussions_page',
    'access arguments' => array('access live discussion page'),
  );
  return $items;
}

function live_discussions_comment_num($nid) {
  $result = db_result(db_query('SELECT COUNT(c.cid) FROM {node} n INNER JOIN {comments} c ON n.nid = c.nid WHERE n.nid = %d AND c.status = %d', $nid, COMMENT_PUBLISHED));
  return $result;
}

function live_discussions_page($uid = NULL) {
  global $user;
  $uid = is_numeric($uid) && user_access('administer live discussions block')? $uid : $user->uid;
  $page_count = variable_get('default_nodes_main', 10);
  // get the date to work with
  $time = getdate();
  $timelimit = gmmktime(23, 59, 59, $time['mon']-1, $time['mday'], $time['year']);

  // create the SQL
  $query = 'SELECT n.nid, n.title, n.type, u.name, cs.comment_count,
            cs.last_comment_timestamp AS the_time, h.timestamp AS last_viewed
            FROM {node} n
            LEFT JOIN {node_comment_statistics} cs ON cs.nid = n.nid
            LEFT JOIN {users} u ON u.uid = n.uid
            LEFT JOIN {history} h ON h.nid = n.nid AND h.uid = ' . $uid .
            ' WHERE n.status = 1 AND cs.comment_count > 0
            AND (cs.last_comment_timestamp > ' . $timelimit . ')

            AND (h.timestamp < cs.last_comment_timestamp OR h.timestamp IS NULL)
            ORDER BY the_time DESC';

  $query_result = pager_query(db_rewrite_sql($query, 'n'), $page_count, 1);

  $pager = theme('pager', NULL, $page_count, 1);
  while($new_commented_node = db_fetch_object($query_result)) {
    $comment_count = live_discussions_comment_num($new_commented_node->nid) . ' comments';
    $new_comment_link = '<br />' . l("$new_commented_node->comment_count new", "node/$new_commented_node->nid", array(), '#new');
  	$rows[] = array(
  	  $new_commented_node->type,
  	  l($new_commented_node->title, "node/$new_commented_node->nid"),
  	  $new_commented_node->name,
  	  "$comment_count $new_comment_link",
  	  format_date($new_commented_node->the_time),
  	);
  }
  $header = array(t('Type'), t('Post'),	t('Author'), t('Replies'),	t('Last post'));
  drupal_set_title(t('Live discussions'));
  $output = theme('table', $header, $rows);
  $output .= $pager;
  return $output;
}
