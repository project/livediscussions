The Live Discussions module creates a block listing recently commented upon threads. The number of nodes listed in the block is configurable (5, 10, 15, 20, 25 or 30 nodes). The link can display the number of comments made on the node and the block title is configurable.

This is different than the block provided in the core distribution because it lists nodes with comments rather than comments.
---------------------------------------------------------------------------
November 2, 2006: The module also provides a page for logged in users to track discussion sitewide. The page is similar to the one at http://drupal.org/tracker but it only lists nodes with comments posted since the user's previous visit. The discovery is limited to one month

This functionality was sponsored by Tony Mobily of FreeSoftware Magazine
http://www.freesoftwaremagazine.com/
---------------------------------------------------------------------------

NOTE: P�ter Cseke noted the following:

Comment count not correct when node_privacy_byrole is installed
P�ter Cseke - November 9, 2005 - 10:43
Project:	Live Discussion
Version:	4.6.0
Component:	Code
Category:	bug
Priority:	normal
Assigned:	Unassigned
Status:	active

Description

When node_privacy_byrole is installed, the comment count for a non-admin user is
greater than the real comment count.

There is a select in the live_discussions module:

      $query = "SELECT c.nid, n.title, COUNT(c.nid) AS comment_count, MAX(c.timestamp) AS the_time FROM {comments} c ".
               "LEFT JOIN {node} n ON n.nid = c.nid ".$select_blog." WHERE c.status = 0 GROUP BY c.nid ORDER BY the_time DESC ".
               "LIMIT ".$commented_limit;

It should be changed to this:

      $query = "SELECT c.nid, n.title, COUNT(DISTINCT c.cid) AS comment_count, MAX(c.timestamp) AS the_time FROM {comments} c ".
               "LEFT JOIN {node} n ON n.nid = c.nid ".$select_blog." WHERE c.status = 0 GROUP BY c.nid ORDER BY the_time DESC ".
               "LIMIT ".$commented_limit;

Notice that in the third column we should select cid instead of nid and because
node_privacy_byrole will duplicate the rows, we must also use DISTINCT inside the
COUNT.
